

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by orrin on 7/25/17.
 */
public class TestArrayList {
    @Test
    public void sizeTestCase() {
        List list = new CustomArrayList();
        Assert.assertTrue(list.size() == 0);
    }

    @Test
    public void isEmptyTestCase() {
        List list = new CustomArrayList();
        Assert.assertTrue(list.size() == 0, "List is not empty!");
    }

    @Test
    public void addTestCase() {
        List list = new CustomArrayList();
        list.add("Black");
        list.add("White test");
        list.add("Red ");
        Assert.assertEquals(list.size(), 3, "Elements not added");
    }

    @Test
    public void clearTestCase() {
        List list = new CustomArrayList();
        list.add("Black");
        list.add("White");
        list.clear();
        Assert.assertTrue(list.size() == 0, "List not cleared");
    }

    @Test
    public void removeNoExistTestCase() {
        List list = new CustomArrayList();
        list.add("test");
        list.add("test2");
        Assert.assertTrue(list.remove("test1"), "There is no such element");
        Assert.assertEquals(2, list.size(), "Incorrect list size");
    }

    @Test
    public void removeTestCase() {
        List list = new CustomArrayList();
        list.add("test");
        list.add("test1");
        list.add("test2");
        list.remove("test2");
        Assert.assertTrue(list.size() == 2, "Incorrect list size after remove element");
        for (Object o : list.toArray()) {
            System.out.println(o);
        }
    }

    @Test
    public void addAllTestCase() {
        List list = new CustomArrayList();
        list.add("Black");
        list.add("White");
        list.add("Red");
        list.addAll(list);
        Assert.assertEquals(list.size(), 3, "Elements not added");
    }
}
