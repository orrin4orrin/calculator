import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pages.AuthPage;
import services.CustomService;
import services.UserDataProvider;

public class LogInWithAccountTest extends BaseTest {
    @Test(dataProvider = "ValidEmail", dataProviderClass = UserDataProvider.class)
    public void loginAccountTest(String email, String pass) throws InterruptedException {
        driver.get("https://account.templatemonster.com/auth/#/");

        AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
        authPage.loginAccount(email, pass);

        Cookie cookieLgn = getCookieLgn();

        Assert.assertEquals(cookieLgn.getValue(), "orrin%40devoffice.com");
    }

    public Cookie getCookieLgn() {
        CustomService.waitForCookie("lgn", driver);
        return driver.manage().getCookieNamed("lgn");
    }
    @AfterMethod
    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
    }
}