import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by orrin on 7/10/17.
 */
public class CalculatorTestSuite {
    @Test
    public void sumResult() throws Exception {
        Calculator calc = new Calculator();
        double res = calc.getResult(2,"+", 4);
        Assert.assertEquals(6.0, res);
    }
    @Test
    public void subtResult() throws Exception {
        Calculator calc = new Calculator();
        double res = calc.getResult(7,"-", 4);
        Assert.assertEquals(3.0, res);
    }
    @Test
    public void multipResult() throws Exception {
        Calculator calc = new Calculator();
        double res = calc.getResult(2,"*", 5);
        Assert.assertEquals(10.0, res);
    }
    @Test
    public void divisResult() throws Exception {
        Calculator calc = new Calculator();
        double res = calc.getResult(12, "/", 5);
        Assert.assertEquals(2.4, res);
    }
    @Test
    public void wrongOperation(){
        Calculator calc = new Calculator();
        try {
            calc.getResult(12, "(", 5);
            Assert.assertTrue(false);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
    }

}


