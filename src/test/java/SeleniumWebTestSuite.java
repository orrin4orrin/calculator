import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import services.CustomService;

public class SeleniumWebTestSuite extends BaseTest {
    @Test
    public void test(){

        CustomService service = new CustomService(driver);

        service.goTo("https://www.templatemonster.com/");

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.waitForHeartEnable();

        homePage.clickOnSignInLink();

        service.switchToLastWindow();
        service.waitUrl("account");

       /* WebElement headerHeart = driver.findElement(By.id("menu-favorites"));
        List<WebElement> mainTabs = driver.findElements(By.xpath(".//*[@class='sub-menu container']//*[contains(@class, 'js-sub-menu-1')]/ul/li"));

        WebElement categoryFashion = driver.findElement(By.id("categories-fashions"));
        WebElement elementHeartOnTemplate = driver.findElement(By.xpath(".//*[@id='tab-bestseller']//*[@class='container']//*[contains(@class, 'templates-listing')]/ul/li[3]//*[contains(@class, 'tm-icon')]"));*/

        Assert.assertEquals(driver.getCurrentUrl(),"https://account.templatemonster.com/auth/#/", "Incorrect URL" );

    }

}
