import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pages.AuthPage;
import pages.FacebookPage;
import pages.HomePage;
import properties.User;
import services.CustomService;

public class LogInWithFacebookTest extends BaseTest {
    @Test
    public void loginFacebookTest () {
        User user = new User();
        CustomService service = new CustomService(driver);
        driver.get("https://account.templatemonster.com/auth/#/");
        AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
        authPage.clickFacebookButton();
        service.switchToLastWindow();
        service.waitUrl("facebook");
        FacebookPage facebookPage = PageFactory.initElements(driver, FacebookPage.class);
        facebookPage.loginFacebook(user);
        service.switchToLastWindow();
//        facebookPage.waitForClickableContinueButton();
//        facebookPage.clickContinueButton();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        service.switchToLastWindow();
        homePage.waitForAccountMenuClickable();

        Cookie cookieLgn = getCookieLgn();

        Assert.assertEquals(cookieLgn.getValue(), "enny-orrinfb%40templatemonster.me");
        }

    public Cookie getCookieLgn() {
        CustomService.waitForCookie("lgn", driver);
        return driver.manage().getCookieNamed("lgn");
    }
    @AfterMethod
    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
    }
}
