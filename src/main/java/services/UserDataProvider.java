package services;

import org.testng.annotations.DataProvider;

public class UserDataProvider {
    @DataProvider(name = "ValidEmail")
    public static java.lang.Object[][] ValidEmail(){
        return new String[][]{{"orrin@devoffice.com", "gh*hyR%$y2"}};
    }
}
