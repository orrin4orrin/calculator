package services;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CustomService {

    private WebDriver driver;

    public CustomService(WebDriver driver) {
        this.driver = driver;
    }

    public void waitClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUrl(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void switchToLastWindow() {
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public void goTo(String url) {
        driver.get(url);
    }

//    public void getURL(String url) {
//        try {
//            driver.get(url);
//        } catch (TimeoutException e) {
//            driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
//        }
//    }

    public static void waitForCookie(final String cookieName, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.manage().getCookieNamed(cookieName) != null);
                }
            });
        }
        catch (TimeoutException e) {
            Assert.assertFalse(true, "failed");
        }
    }
}
