import java.util.Scanner;

/**
 * Created by orrin-orrin on 7/10/17.
 */
public class Calculator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the first number");
        double number1 = sc.nextDouble();
        System.out.println("Enter the second number");
        double number2 = sc.nextDouble();
        System.out.println("Enter operation symbol");
        String operation = sc.next();
        System.out.println("You entered " + number1 + operation + number2 + " =");
        sc.close();
        try  {
            double res = getResult(number1, operation, number2);
            System.out.print(res);
        } catch(Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public static double getResult(double number1, String operation, double number2) throws Exception {
        switch (operation) {
            case "+":
                return number1 + number2;
            case "-":
                return number1 - number2;
            case "*":
                return number1 * number2;
            case "/":
                return number1 / number2;
            default:
                throw new Exception("Invalid operation. Please, enter +, - , * , /");
        }
    }

//        if (operation.equals("+")){
//            res = number1 + number2;
//        }
//        else if (operation.equals("-")) {
//            res = number1 - number2;
//        }
//        else if (operation.equals("*")) {
//            res = number1 * number2;
//        }
//        else if (operation.equals("/")) {
//            res = number1 / number2;
//        }
//        else {
//        }
//        System.out.println(res);

}