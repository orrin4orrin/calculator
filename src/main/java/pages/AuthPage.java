package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import properties.User;
import services.CustomService;

public class AuthPage {
    private final WebDriver driver;
    private CustomService service;

    public AuthPage(WebDriver driver){
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(xpath = ".//input[@type='email']")
    private WebElement emailField;

    @FindBy(id="id-index-continue-button")
    private WebElement continueButton;

    @FindBy(xpath = ".//input[@type='password']")
    private WebElement passwordField;

    @FindBy(id="id-password-login-button")
    private WebElement loginButton;

    @FindBy(xpath = ".//*[@class='notification__container']")
    private WebElement errorNotification;

    @FindBy(id = "id-general-facebook-button")
    WebElement facebookLoginButton;


    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void clickContinue(){
        continueButton.click();
    }

    public void waitForLoginClicable(){
        service.waitClickable(loginButton);
    }

    public void enterPassword(String pass){
        passwordField.sendKeys(pass);
    }

    public void clickLogIn(){
        loginButton.click();
    }

    public void loginAccount(String email, String pass){
        enterEmail(email);
        clickContinue();
        enterPassword(pass);
        clickLogIn();
    }

    public void clickFacebookButton(){
        facebookLoginButton.click();
    }

}
