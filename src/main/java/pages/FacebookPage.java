package pages;

import junit.runner.BaseTestRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.internal.BaseTestMethod;
import properties.User;
import services.CustomService;

public class FacebookPage {
    private final WebDriver driver;
    private CustomService service;

    public FacebookPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "pass")
    private WebElement passwordField;

    @FindBy(id = "loginbutton")
    WebElement loginButton;

    @FindBy(xpath = ".//*[contains(@class, 'selected')]")
    WebElement continueButton;

    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void enterPassword(String pass){
        passwordField.sendKeys(pass);
    }

    public void loginFacebook(User user){
        enterEmail(user.getEmail());
        enterPassword(user.getPass());
        loginButton.click();
    }

    public void waitForClickableContinueButton(){
        service.waitClickable(continueButton);
    }

    public void clickContinueButton(){
        continueButton.click();
    }
}
