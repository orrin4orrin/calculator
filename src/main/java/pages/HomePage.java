package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;

import java.util.List;

public class HomePage {
    private final WebDriver driver;
    private CustomService service;

    public HomePage(WebDriver driver){
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy (id = "menu-favorites")
    private WebElement heart;

    @FindBy (id = "header-signin-link")
    private WebElement signInLink;

    @FindBy (id = "categories")
    private List<WebElement> categories;

    @FindBy (id = "app-account-menu")
    WebElement accountMenu;

    public void waitForHeartEnable(){
        service.waitClickable(heart);
    }

    public void clickOnSignInLink(){
        signInLink.click();
    }

    public void waitForAccountMenuClickable(){
        service.waitClickable(accountMenu);
    }





}
